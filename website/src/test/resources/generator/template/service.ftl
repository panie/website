package ${basePackage}${modulePackage}.service;
import ${basePackage}${modulePackage}.model.${modelNameUpperCamel};
import ${basePackage}.core.Service;


/**
 * Created by ${author} on ${date}.
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {

}
