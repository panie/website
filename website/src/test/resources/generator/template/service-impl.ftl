package ${basePackage}${modulePackage}.service.impl;

import ${basePackage}${modulePackage}.dao.${modelNameUpperCamel}Mapper;
import ${basePackage}${modulePackage}.model.${modelNameUpperCamel};
import ${basePackage}${modulePackage}.service.${modelNameUpperCamel}Service;
import ${basePackage}.core.AbstractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ${author} on ${date}.
 */
@Service
public class ${modelNameUpperCamel}ServiceImpl extends AbstractService<${modelNameUpperCamel}> implements ${modelNameUpperCamel}Service {
    private static final Logger logger = LoggerFactory.getLogger(${modelNameUpperCamel}ServiceImpl.class);

    @Resource
    private ${modelNameUpperCamel}Mapper ${modelNameLowerCamel}Mapper;

}
