package com.panie.site.website.service;
import com.panie.site.website.model.StType;
import com.panie.site.core.Service;


/**
 * Created by CodeGenerator on 2020/01/17.
 */
public interface StTypeService extends Service<StType> {

}
