package com.panie.site.website.service.impl;

import com.panie.site.website.dao.StSiteMapper;
import com.panie.site.website.model.StSite;
import com.panie.site.website.service.StSiteService;
import com.panie.site.core.AbstractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * Created by CodeGenerator on 2020/01/17.
 */
@Service
public class StSiteServiceImpl extends AbstractService<StSite> implements StSiteService {
    private static final Logger logger = LoggerFactory.getLogger(StSiteServiceImpl.class);

    @Resource
    private StSiteMapper stSiteMapper;

    @Override
    public List<StSite> findByTypeId(Integer typeId) {
        Condition condition = new Condition(StSite.class);
        Example.Criteria criteria = condition.createCriteria();
        criteria.andEqualTo("typeId", typeId);
        return  mapper.selectByCondition(condition);
    }
}
