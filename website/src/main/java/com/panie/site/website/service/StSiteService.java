package com.panie.site.website.service;
import com.panie.site.website.model.StSite;
import com.panie.site.core.Service;

import java.util.List;


/**
 * Created by CodeGenerator on 2020/01/17.
 */
public interface StSiteService extends Service<StSite> {

    List<StSite> findByTypeId(Integer typeId);
}
