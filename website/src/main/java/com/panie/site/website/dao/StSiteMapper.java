package com.panie.site.website.dao;

import com.panie.site.core.Mapper;
import com.panie.site.website.model.StSite;

public interface StSiteMapper extends Mapper<StSite> {
}