package com.panie.site.website.service.impl;

import com.panie.site.website.dao.StTypeMapper;
import com.panie.site.website.model.StType;
import com.panie.site.website.service.StTypeService;
import com.panie.site.core.AbstractService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by CodeGenerator on 2020/01/17.
 */
@Service
public class StTypeServiceImpl extends AbstractService<StType> implements StTypeService {
    private static final Logger logger = LoggerFactory.getLogger(StTypeServiceImpl.class);

    @Resource
    private StTypeMapper stTypeMapper;

}
