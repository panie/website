package com.panie.site.website.dao;

import com.panie.site.core.Mapper;
import com.panie.site.website.model.StType;

public interface StTypeMapper extends Mapper<StType> {
}