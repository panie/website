package com.panie.site.website.web;

import com.panie.site.core.Result;
import com.panie.site.core.ResultGenerator;
import com.panie.site.website.model.StSite;
import com.panie.site.website.service.StSiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by CodeGenerator on 2020/01/17.
 */
@RestController
@RequestMapping("/site/site")
public class StSiteController {
    private static final Logger logger = LoggerFactory.getLogger(StSiteController.class);

    @Resource
    private StSiteService stSiteService;

    @RequestMapping("/add")
    public Result add(@RequestBody StSite stSite) {
        stSiteService.save(stSite);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/delete")
    public Result delete(@RequestBody StSite stSite) {
        stSiteService.deleteById(stSite.getId());
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/update")
    public Result update(@RequestBody StSite stSite) {
        stSiteService.update(stSite);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        StSite stSite = stSiteService.findById(id);
        return ResultGenerator.genSuccessResult(stSite);
    }

    @RequestMapping("/list")
    public Result list(@RequestParam(required = false) Integer typeId) {
        List<StSite> list = null;
        if (typeId == null) {
            list = stSiteService.findAll();
        } else {
            list = stSiteService.findByTypeId(typeId);
        }
        if (list == null) {
            list = Collections.EMPTY_LIST;
        }
        list.sort(Comparator.comparing(e -> e.getSort()));

        return ResultGenerator.genSuccessResult(list);
    }
}
