package com.panie.site.website.model;

import javax.persistence.*;

@Table(name = "st_site")
public class StSite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "site_name")
    private String siteName;

    @Column(name = "site_icon")
    private String siteIcon;

    /**
     * 网站url
     */
    @Column(name = "site_url")
    private String siteUrl;

    @Column(name = "site_desc")
    private String siteDesc;

    private String remark;

    /**
     * 类别ID
     */
    @Column(name = "type_id")
    private Integer typeId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return site_name
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * @param siteName
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * @return site_icon
     */
    public String getSiteIcon() {
        return siteIcon;
    }

    /**
     * @param siteIcon
     */
    public void setSiteIcon(String siteIcon) {
        this.siteIcon = siteIcon;
    }

    /**
     * 获取网站url
     *
     * @return site_url - 网站url
     */
    public String getSiteUrl() {
        return siteUrl;
    }

    /**
     * 设置网站url
     *
     * @param siteUrl 网站url
     */
    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    /**
     * @return site_desc
     */
    public String getSiteDesc() {
        return siteDesc;
    }

    /**
     * @param siteDesc
     */
    public void setSiteDesc(String siteDesc) {
        this.siteDesc = siteDesc;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取类别ID
     *
     * @return type_id - 类别ID
     */
    public Integer getTypeId() {
        return typeId;
    }

    /**
     * 设置类别ID
     *
     * @param typeId 类别ID
     */
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }
}