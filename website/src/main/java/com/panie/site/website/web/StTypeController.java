package com.panie.site.website.web;

import com.panie.site.core.Result;
import com.panie.site.core.ResultGenerator;
import com.panie.site.website.model.StType;
import com.panie.site.website.service.StTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by CodeGenerator on 2020/01/17.
 */
@RestController
@RequestMapping("/site/type")
public class StTypeController {
    private static final Logger logger = LoggerFactory.getLogger(StTypeController.class);

    @Resource
    private StTypeService stTypeService;

    @RequestMapping("/add")
    public Result add(@RequestBody StType stType) {
        stTypeService.save(stType);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/delete")
    public Result delete(@RequestBody StType stType) {
        stTypeService.deleteById(stType.getId());
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/update")
    public Result update(@RequestBody StType stType) {
        stTypeService.update(stType);
        return ResultGenerator.genSuccessResult();
    }

    @RequestMapping("/detail")
    public Result detail(@RequestParam Integer id) {
        StType stType = stTypeService.findById(id);
        return ResultGenerator.genSuccessResult(stType);
    }

    @RequestMapping("/list")
    public Result list() {
        List<StType> list = stTypeService.findAll();
        list.sort(Comparator.comparing(e -> e.getSort()));
        return ResultGenerator.genSuccessResult(list);
    }
}
