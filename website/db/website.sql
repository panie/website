/*
Navicat MySQL Data Transfer

Source Server         : 腾讯云(118.24.12.71)
Source Server Version : 50639
Source Host           : 118.24.12.71:3306
Source Database       : website

Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001

Date: 2020-01-20 18:00:21
*/

create database if not exists website;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for st_site
-- ----------------------------
DROP TABLE IF EXISTS `st_site`;
CREATE TABLE `st_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(50) DEFAULT NULL,
  `site_icon` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL COMMENT '网站url',
  `site_desc` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL COMMENT '类别ID',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `index_1` (`type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for st_type
-- ----------------------------
DROP TABLE IF EXISTS `st_type`;
CREATE TABLE `st_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `tpye_code` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
