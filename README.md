# website

#### 介绍
一个简单的导航网站，用于收集自己常用的导航网址。

#### 软件架构
软件架构说明 http://doc.panpanie.com/uploads/website/images/m_682596680283e44ae98b5b32019350ee_r.png

采用了前后端分离

前端源码： webroot

后端源码： website


#### 安装教程

1.  导入数据库脚本

2.  编译webroot 项目，可以借助 nginx 来部署

3.  配置好数据库配置文件，打包 website，将生成的jar 运行

    ​

#### 使用说明

1.  效果预览 <http://site.panpanie.com/#/>
